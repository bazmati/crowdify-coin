contract CrowdifyCrowdSale {
    mapping (bytes32 => uint) public investmentLedger; //Map Twitter name to amount purchased
    mapping (bytes32 => bool) public migrated; //Whether or not the user has transferred their original coins to the main contract
    uint public deadline;
    //address public owner;
    bytes32[] public investorIdList;
    uint public investorCount;
    address[] public owners;
    uint public totalCoinsSold; 

    function CrowdifyCrowdSale(uint durationInHours) 
    {
        owners.push(msg.sender);
        deadline = now + durationInHours * 1 hours;
    } 
    
    event SaleMade(bytes32 investorId, uint256 numberOfCoins);
    
    modifier onlyOwner() {
        //if (!isOwner(msg.sender)) throw;
        _
    }
    
    modifier onlyBeforeDeadline() {
        if (now >= deadline) throw;
        _
    }

    function kill() onlyOwner {
        suicide(msg.sender);
    }
    
    function isOwner(address owner) returns (bool) {
        uint i = 0;
        bool found = false;
        for (i=0; i<owners.length; i++) {
            if (owners[i] == owner) {
                found = true;
                break;
            }
        }
        return found;
    }

    function addOwner(address newOwner) onlyOwner {
       owners.push(newOwner); 
    }
    
    function removeOwner(address ownerToRemove) onlyOwner {
        if (owners.length == 1) {
            throw; //Can't remove all owners
        }
        else {
            uint i = 0;
            for (i=0; i<owners.length; i++) {
                if (owners[i] == ownerToRemove) {
                    delete owners[i];
                    break;
                }
            }
        }          
    }

    function registerSale (bytes32 investorId, uint numberOfCoins) onlyOwner onlyBeforeDeadline {
        if (investmentLedger[investorId] == 0)
        {
            investorIdList.push(investorId);
            investorCount ++;
        }
        investmentLedger[investorId] += numberOfCoins; 
        totalCoinsSold += numberOfCoins;
        SaleMade(investorId, numberOfCoins);
    }
    
    function markAsMigrated (bytes32 investorId) onlyOwner {
        migrated[investorId] = true;
    }
    
    
    //Sending ether to this contract is not allowed
    function () {
        throw;
    }
}