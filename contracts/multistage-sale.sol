contract CrowdifyCrowdSale {

    address[] public owners;
    
    mapping (bytes32 => uint) public investmentLedger; //Map Twitter name to amount purchased
    mapping (bytes32 => bool) public migrated; //Whether or not the user has transferred their original coins to the main contract
   
    uint public deadline;
    uint public startTime;

    bytes32[] public investorIdList;
    uint public investorCount;

    mapping (uint => Stage) public stages;
    uint public stageCount;
    Stage public currentStage;
    Stage public nextStage;
    uint secondsUntilNextStage;
    bool public finished;
    uint public intrusionCount;
    
    uint public totalCoinsSold;
    
    struct Stage {
        uint index;
        uint startLine;
        uint price;
    }

    function CrowdifyCrowdSale(uint durationInHours, uint initialPrice) 
    {
        owners.push(msg.sender);
        startTime = now;
        setDeadline(durationInHours);
        addStage(0, initialPrice);
    } 
    
    function setDeadline(uint hoursFromStart) onlyOwner {
        deadline = startTime + hoursFromStart * 1 hours;
        if (deadline > now) {
            finished = false;
        }
        else {
            finished = true;
        }
    }
    
    event SaleMade(bytes32 investorId, uint numberOfCoins);
    
    modifier onlyOwner() {
        if (!isOwner(msg.sender)) {
            intrusionCount ++;
        }
        else
        _
    }
    
    modifier onlyBeforeDeadline() {
        if (now >= deadline) {
            finished = true;
        }
        if (now < deadline)
        _
    }
    

    function kill() onlyOwner {
        suicide(msg.sender);
    }
    
    function isOwner(address owner) returns (bool) {
        uint i = 0;
        bool found = false;
        for (i=0; i<owners.length; i++) {
            if (owners[i] == owner) {
                found = true;
                break;
            }
        }
        return found;
    }

    function addOwner(address newOwner) onlyOwner {
       owners.push(newOwner); 
    }
    
    function removeOwner(address ownerToRemove) onlyOwner {
        if (owners.length == 1) {
            throw; //Can't remove all owners
        }
        else {
            uint i = 0;
            for (i=0; i<owners.length; i++) {
                if (owners[i] == ownerToRemove) {
                    delete owners[i];
                    break;
                }
            }
        }          
    }
    

    
    function addStage(uint startLine, uint price) onlyBeforeDeadline onlyOwner {
        if (startTime + startLine > deadline) throw;
        stages[stageCount] = Stage({
            index: stageCount,
            startLine: startLine,
            price: price
        });
        stageCount ++;
    }

    
    function getCurrentStage() onlyBeforeDeadline private returns (Stage) {
        var nowTime = now;
        bool found = false;
        currentStage = stages[0];
        for (var i=stageCount-1; i>=0; i--) {
            var timeElapsed = nowTime - startTime;
            if (stages[i].startLine < timeElapsed) {
               currentStage = stages[i];
               break;
            }
        }
        return currentStage;

    }
    
    function getNextStage() onlyBeforeDeadline private returns (Stage) {
        var current = getCurrentStage();
        if (current.index < stageCount-1) {
            nextStage = stages[current.index+1];
            return nextStage;
        }
        else throw;
    }
    
    function getSecondsUntilNextStage() onlyBeforeDeadline returns (uint) {
        secondsUntilNextStage = (startTime + getNextStage().startLine) - now;
        return secondsUntilNextStage;
    }
    
    function getCurrentStageIndex() onlyBeforeDeadline returns (uint) {
        getCurrentStage().index;
    }
    
    function getCurrentPrice() onlyBeforeDeadline returns (uint) {
        return getCurrentStage().price;
    }
    
    function getPriceAtNextStage() onlyBeforeDeadline returns (uint) {
        return getNextStage().price;
    }
    
    function registerSale (bytes32 investorId, uint cents) onlyBeforeDeadline onlyOwner {
        var currentStage = getCurrentStage();
        uint coins = cents / currentStage.price;
        assignCoins(investorId, coins);
    }
    
    function assignCoins(bytes32 investorId, uint numberOfCoins) onlyOwner private {
        if (investmentLedger[investorId] == 0)
        {
            investorIdList.push(investorId);
            investorCount ++;
        }
        investmentLedger[investorId] += numberOfCoins; 
        totalCoinsSold += numberOfCoins;
        SaleMade(investorId, numberOfCoins);
    }
    
    function markAsMigrated (bytes32 investorId) onlyOwner {
        migrated[investorId] = true;
    }
    
    //Sending ether to this contract is not allowed
    function () {
        throw;
    }
}