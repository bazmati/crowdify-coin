//Linode test server 45.79.83.93

var Express = require('express');
var Http = require('http');
var Https = require('https');
//var QueryString = require('querystring');
var BodyParser = require('body-parser');
var Utils = require('./utils.js');
var CrowdSale = require('./crowdSale.js');
var Exchange = require('./exchange.js');
var Fs = require('fs');


//=== App
var app = Express();
app.use(BodyParser.json());
app.use(Utils.allowCrossDomain);
app.use(Utils.logRequest);

app.set('port', Utils.PORT);

app.get('/', function(req, res)
{
	res.status(200).send("Crowdify Coin API 0.1");
});


//== User
//app.post('/block.io', CrowdSale.btcPayment);
app.get('/btcrate', Exchange.btcRate);
//app.post('/1022/stripe/crowdsale', CrowdSale.payment);
//app.post('/stripe/token', CrowdSale.charge);
app.get('/crowdsale/:investorId', CrowdSale.getBalance);
app.get('/crowdsale', CrowdSale.getStatus);
app.post('/73erf9011p5', CrowdSale.payment);
app.use(Express.static('../client'));

//Start server
Http.createServer(app).listen(80, function(){
	console.log('HTTP Web service listening on port 80');
});

var privateKey = Fs.readFileSync( '../security/server.key' );
var certificate = Fs.readFileSync( '../security/server.crt' );
Https.createServer({ cert: certificate,	key: privateKey}, app).listen(443, function(){
	console.log('HTTPS Web service listening on port 443');
});

//Exception safety net
process.on('uncaughtException', function(err) {
	// handle the error safely
	console.log("SAFETY NET", err.stack);
});

