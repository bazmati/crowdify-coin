var fs = require('fs');

var allowCrossDomain = function(req, res, next) {
	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE');
	res.header('Access-Control-Allow-Headers', 'Content-Type,apiKey,appId,adminKey,userId,token');

	next();
};
exports.allowCrossDomain = allowCrossDomain;

function handleResponse(err, data, res, errCode)
{
	if (err)
	{
		if (!errCode)
		{
			errCode = 500;
		}
		console.error(err);
		log('errors.log', err);
		res.status(errCode).json({"error": err});
	}
	else
	{
		if (data)
		{
			if (data.toJSON)
			{
				data = data.toJSON();
			}
			res.status(200).json(data);
		}
		else
		{
			res.status(404).json({"error": "Item not found"});
		}
	}
}
exports.handleResponse = handleResponse;

function logRequest(req, res, next) {
	console.log(req.ip, req.originalUrl);
	log('requests.csv', new Date().toString() + "," + req.ip + "," + req.originalUrl + "," + req.urlArgs );
	next();
};
exports.logRequest = logRequest;

function log(fileName, message) {
	fs.appendFile(fileName, message);
}
exports.log = log;