var Utils = require('./utils');
var Fs = require('fs');
var Request = require('request');

function btcRate(req, res) {
    getRate(function(err, data) {
        Utils.handleResponse(err, data, res);
    });
}
exports.btcRate = btcRate;

function updateRate(callback) {
    if (typeof(callback) !== "function") {
        callback = function(){};
    }
    var start = Math.round((new Date().getTime() - 3000000 * 2)/1000);
    var url = "https://poloniex.com/public?command=returnChartData&currencyPair=USDT_BTC&start="+start+"&end=9999999999&period=300";
    Request(url, function (err, response, body) {
        if (err) {
            callback(err);
        }
        else {
            var jsonData = JSON.parse(body);
            var rate = jsonData[jsonData.length-1];
            console.log("New rate", rate.weightedAverage);
            Fs.writeFile('rate.json', JSON.stringify(rate), 'utf8', function(err) {
                callback(err, rate);
            });
            callback(err);           
        }
    });
}
exports.updateRate = updateRate;

function getRate(callback) {
    if (typeof(callback) !== "function") {
        callback = function(){};
    }
    Fs.readFile('rate.json', 'utf8', function(err, data) {
        if (err) {
            callback(err);
        }
        else {
            var jsonData = JSON.parse(data);
            console.log("Current rate", jsonData.weightedAverage);
            callback(null, jsonData.weightedAverage);
        }
    })
}

setInterval(updateRate, 300000);


