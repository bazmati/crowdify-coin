
var Ethereum = require('./ethereum.js');
var Utils = require('./utils.js');
var Async = require('async');
var Settings = require('./settings.js');
var Exchange = require('./exchange.js');
var Request = require('request');
var BLOCK_IO_API_KEY = 'a0ee-f96b-f5b3-53bf';

//var Stripe = require("stripe")(Settings.stripeLiveSecret);

exports.getBalance = function(req, res)
{
    //Get contract
    
    Ethereum.contracts.crowdSale.investmentLedger.call(req.params.investorId, function(err, data)
    {
        if (data && typeof(data) == "object" && data.toNumber) {
            data = {
                investorId: req.params.investorId,
                numberOfCoins: data.toNumber() 
            }    
        }
        Utils.handleResponse(err, data, res);
    });
   
};

function dateBreakdown(ms) {
    var delta = ms/1000;
    
    var result = {
        totalSeconds: delta
    };
    
    // calculate (and subtract) whole days
    result.days = Math.floor(delta / 86400);
    delta -= result.days * 86400;
    
    // calculate (and subtract) whole hours
    result.hours = Math.floor(delta / 3600) % 24;
    delta -= result.hours * 3600;
    
    // calculate (and subtract) whole minutes
    result.minutes = Math.floor(delta / 60) % 60;
    delta -= result.minutes * 60;
    
    // what's left is seconds
    result.seconds = Math.round(delta);
    
    return result;
}

exports.getStatus = function(req, res)
{
    var result = {
        totalCoinsSold: 0,
        timeRemaining: {},
        investorCount: 0,
        nextStageStartTime: 0,
        priceAtNextStage: 0,
        currentPrice: 0,
        deadline: 0
    };
    Async.parallel(
        [
            function _totalCoinsSold(callback) {
                Ethereum.contracts.crowdSale.totalCoinsSold.call(function(err, data)
                {
                    if (data && typeof(data) == "object" && data.toNumber) {
                        result.totalCoinsSold = data.toNumber();
                    }
                    callback(err);
                });
                 
            },
            function _timeRemaining(callback) {
                Ethereum.contracts.crowdSale.deadline.call(function(err, data)
                {
                    if (data && typeof(data) == "object" && data.toNumber) {
                        var delta = (data.toNumber()*1000) - new Date().getTime();
                        result.timeRemaining = dateBreakdown(delta);
                        result.deadline = new Date(delta + new Date().getTime()).getTime();
                    }
                    callback(err);
                });              
            },
            function _currentPrice(callback) {
                Ethereum.contracts.crowdSale.getCurrentPrice.call(function(err, data) {
                    if (data && typeof(data) == "object" && data.toNumber) {
                        result.currentPrice = data.toNumber();
                    }
                    callback(err);
                });
            },
            function _secondsUntilNextStage(callback) {
                Ethereum.contracts.crowdSale.getSecondsUntilNextStage.call(function(err, data) {
                    if (data && typeof(data) == "object" && data.toNumber) {
                        result.nextStageStartTime = new Date().getTime() + (data.toNumber() * 1000);
                    }
                    callback(err);
                });
   
            },
            function (callback) {
                Ethereum.contracts.crowdSale.getPriceAtNextStage.call(function(err, data) {
                    if (data && typeof(data) == "object" && data.toNumber) {
                        result.priceAtNextStage = data.toNumber();
                    }
                    callback(err);
                });                
            },
            function _investorCount(callback) {
                 Ethereum.contracts.crowdSale.investorCount.call(function(err, data)
                {
                    if (data && typeof(data) == "object" && data.toNumber) {
                        result.investorCount = data.toNumber();
                    }
                    callback(err);
                });               
            }
        ],
        function(err) {
            Utils.handleResponse(err, result, res);
        }
    )

};


exports.payment = function(req, res) {
    if (req.body) {
        console.log(JSON.stringify(req.body));
    }

    //Get amount paid, 
    var usdRate = 536.28; //Default, emergency fallback which should never be used

    Async.parallel(
        [
            function _getRate(callback) {
                Exchange.getRate(function(err, rate) {
                    usdRate = rate;
                    callback(err);
                });
            }
        ],
        
        function(err) {
            if (err) 
            {
                Utils.handleResponse(err, null, res);
            }
            else {
                if (!req.body.usdCents) {
                    req.body.usdCents = Math.round(req.body.btcAmount * usdRate) ;//req.body.data.amount_received * usdRate);
                }
                Ethereum.contracts.crowdSale.registerSale.sendTransaction(req.body.investorId, req.body.usdCents, {to: Ethereum.contractData.crowdSale.address, gas: 300000}, function(err, data) {
                    if (err) {
                        console.error("Error registering sale:", err);
                    }
                    console.log("Register sale response", data);
                    Utils.handleResponse(err, data, res);  
                });                 
            }
         
        }
    );       

};

