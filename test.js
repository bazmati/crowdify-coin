console.log("Let's do some Ethereum stuff.");
var Web3 = require('web3');
var web3 = new Web3(new Web3.providers.HttpProvider("http://localhost:8545"));
var baz = "fa063c7a16b425352b7b979fd9ddaf853d2cc996";
var mist = "ACB010490F2D1423cE0889a3A0AA099e202810A8";
var other = "51342e1da9cc21b49b312e46f9c9fd1ed3a6bb16";
var ivan = "6557fa61Bc16A70FF4DC69A7797421D63898399F";
/*web3.eth.sendTransaction({from: baz, to: mist, amount: 100000000000000000}, function(err) {
    if (err) {
        console.log(err);
    }
    else {
        console.log("Sent successfully");
    }
    //process.exit();
});*/
var accounts = [baz, other, mist, ivan];
accounts.forEach(function(item) {
    var balance = web3.eth.getBalance(item);
    console.log(item, "balance:", balance);   
})


/*console.log("Getting latest block");
web3.eth.getBlock(
    "latest",
    function(err, result)
    { 
        if (err) console.error(err);
        console.log(result.number, result.hash); 
    }
);*/

var fs = require('fs');
console.log("Compiling contract");
var contractSource = fs.readFileSync('./contracts/crowd-sale.sol', {encoding: 'utf8'});
var compiled = web3.eth.compile.solidity(contractSource);
var Contract = web3.eth.contract(compiled.greeter.info.abiDefinition);
console.log("Creating contract with account", web3.eth.accounts[0]);

var contract = Contract.new(
    8000,
    {
        from:web3.eth.accounts[0],
        data: compiled.CrowdifyCrowdSale.code,
        gas: 2000
    }, 
    function(err, contract){
        if(err) {
            console.error(err);
        }
        else {

            if (!contract.address) {
                console.log("Contract transaction send: TransactionHash: " + contract.transactionHash + " waiting to be mined...");
            } 
            else {
              console.log("Contract mined! Address: " + contract.address);
              console.log(contract);
            }
        }
        process.exit();
    }
);

